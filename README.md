
## Installation

```
cd IshmaelTakeHome
```
install the necessary packages locally
```
npm install | yarn
```
and start up a local server
```
npm start | yarn start
```
Now visit [`localhost:3000`](http://localhost:3000) from your browser. Now your app should be up and running.

This app makes use of a package called faker to get fake data and populate the cards with 
